# freeCodeCamp Forum Dark Theme

**Note**: freeCodeCamp's forum now has its own built-in dark theme. Read the [announcement](https://forum.freecodecamp.org/t/i-designed-a-dark-theme-night-mode-you-can-easily-toggle-on-and-off/183711) for more information.

This style has been written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

## How to install

If you have Stylus installed, click on the banner below and a new window of Stylus should open, asking you to confirm to install the style. For other style managers, you may need to copy and paste the raw css code and update manually.

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/freecodecamp-forum-dark-theme/raw/master/freecodecamp-forum-dark-theme.user.css)

If the direct link above doesn't work, please, select freecodecamp-forum-dark-theme.user.css in the list of files then click on "Raw" or "Open Raw" (depending on whether you're on GitHub or GitLab).

## Screenshots

![A screenshot showing the main page](screenshot1.png)

![A screenshot showing a thread](screenshot2.png)
